#!/usr/bin/python3
# -*- coding: UTF-8 -*-

#MasterScript
#1) Launch ExhaustMut in order to get all mutants
#2) For each perturbation file file generated,
#   make the simulations using the Attractors_tables.py script and GINsim!
#3) Use the IsolatorMapper.R script in order to get plenty of heatmaps.

##############################################
#                                            #
#               Import Part                  #
#                                            #
##############################################

import os
import sys
import subprocess
from pathlib import Path
import pandas as pd
print (sys.version)
##############################################
#                                            #
#    Script part for making all simulations  #
#                                            #
##############################################



def displayNumbers(l):
    i = 0
    for e in l:
        print("%i : %s" % (i+1 , e))
        i += 1
    return()




# Get all perturbations files before using them in GINsim
def detectAllPerturbations(comp,perturb,mutNature):
    all_perturbations = []
    source_path = Path(__file__).parent
    path_comp = str((source_path / comp).resolve())
    path_perturb = str((source_path / perturb).resolve())
    path_exhaust = str((source_path / "ExhaustMut.py").resolve())
    #command = '""python ExhaustMut.py %s %s %s""' % (path_comp,path_perturb,mutNature)
    command = [sys.executable, path_exhaust, path_comp, path_perturb,mutNature]
    print("\n" , command, "\n")

    #test = subprocess.check_output(command, stderr = subprocess.STDOUT)
    try:
        subprocess.check_output(command, stderr = subprocess.STDOUT, universal_newlines = True)
        print("Launching command...")

    except subprocess.CalledProcessError as e:
            print ("error>",e.output,'<', flush = False)
            print ("Whoops... Looks like the perturbation files have not been generated!")
    for e in os.listdir(cwd):
        if "Mutants_list_" in e:
            all_perturbations += [e]
    if len(all_perturbations) == 0:
        print("No perturbation file has been found...")
    else:
        print("Perturbations files = Acquired!")
        return(sorted(all_perturbations))

# Simulate perturbations with GINsim and the generated files from Exhaustmut.py
def perturbationsSimulator(model,GINsim,comp,perturb,mutNature):

    all_perturbations = detectAllPerturbations(comp,perturb,mutNature)

    #command = [sys.executable, path_exhaust, path_comp, path_perturb,mutNature]

    print(all_perturbations)
    script = 'Attractors_tables.py'

    for e in all_perturbations:

        command = ["java","-jar", GINsim, "-s", script, model, "-p" , e]
        print(command)

        try:
            subprocess.check_output(command, stderr = subprocess.STDOUT, universal_newlines = True)
        except subprocess.CalledProcessError as e:
            print ("error>",e.output,'<')
    print("All simulations are done.")


##Launch the masterscript using the informations coming from Rshiny UI.
print(sys.argv)
ginsimver = sys.argv[1]
modelpath = sys.argv[2]
model_comp_path = sys.argv[3]
perturb_file = sys.argv[4]
Nature_Mut = sys.argv[5]


cwd = os.getcwd()
print(cwd)
perturbationsSimulator(modelpath,ginsimver,model_comp_path,perturb_file,Nature_Mut)
#detectAllPerturbations(model_comp_path,perturb_file,Nature_Mut)
