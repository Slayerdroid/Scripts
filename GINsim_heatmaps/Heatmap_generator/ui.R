#
# This is the user-interface definition of a Shiny web application. You can
# run the application by clicking 'Run App' above.
#
# Find out more about building applications with Shiny here:
# 
#    http://shiny.rstudio.com/
#

# Define UI for application which performs GINsim simulations and heatmap generation 
dashboardPage(
  dashboardHeader(title = "Heatmap Generator"),
  dashboardSidebar(
    sidebarMenu(
      menuItem("GINsim", 
               tabName = "GINsim",
               icon = icon("dna")),
      menuItem("Simulation results analysis", 
               tabName = "Analysis", 
               icon = icon("layer-group"))
    )
  ),
  
  dashboardBody(
    tabItems( #GINsim Analysis
      tabItem(tabName = "GINsim",
              
              box(width = 12,
              fileInput("file1", "Choose GINsim File",
                        multiple = FALSE,
                        accept = c(".ginml",
                                   "SBML file",
                                   ".zginml"))
              )
              ,
              # For Diagnosis
              #fluidRow(box(tableOutput("file"))),
              #fluidRow(textOutput("command")),
              

              uiOutput("ibox"), # Info about the model nodes
              
              
              uiOutput("sim_params") # Choose the perturbations parameters for GINsim
                
              ,

              uiOutput("launcher") # Button  to perform the whole perturbations
              # The comment below = diagnosis
              #fluidRow(box(verbatimTextOutput("model_compendium"))),
              
              
      ),
      tabItem(tabName = "Analysis", h1("Simulation results analysis"),
              fluidRow(
                uiOutput("par_analysis"), # Parameters for the heatmap generation
                uiOutput("pdf_generator"), # Button for the heatmaps generation
                uiOutput("download"), # Button for downloading the simulation results
                textOutput("model_inp")
                #plotOutput("plot", width = "100%") # Display a sample plot (the WT one for the first output chosen)
              )
      )
    )
    
    
  )
)

  
 