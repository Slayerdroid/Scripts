#### All functions to make the analysis <3 


#### Reading and formatting functions ----

#' Read all csv files from the directory containing GINsim Results
#'
#' @param chem1 the directory path
#' @return a list of dataframes corresponding to the directory path (a list of mutants in that case)

reader <- function(chem1) #chem1 = Directory containing GINsim results for simple, double, "nième" mutants
{
  tp <- list.files(chem1)
  chemtp <- paste(chem1,"/",tp,sep = "")
  return(lapply(chemtp, read.table, sep = "," ,stringsAsFactors = FALSE, header = F))
}

#' Read all csv files from one directory containing GINsim Results (mutants of interest)
#'
#' @param chem1 the directory path
#' @param MutIS the mutants names corresponding of the mutants of interest
#' @return a list of dataframes corresponding to the directory path (a list of mutants of interest)
reader2 <- function(chem1, MutIS)
{
  #print(chem1)
  tp <- list.files(chem1)
  tp2 <- get.IS.filenames(tp,MutIS)
  #print(tp2)
  chemtp <- paste(chem1,"/",tp2,sep = "")
  #print(chemtp)
  return(lapply(chemtp, read.table, sep = "," ,stringsAsFactors = FALSE, header = F))
}


#' Extract the mutant name
#'
#' @param mot the filename
#' @return the extracted mutant name

extractor <- function(mot)
{
  xx <- substring(mot, gregexpr(pattern = "__", mot)[[1]][[1]]+2)
  #Remove the mutant tag (KO or E1)
  xy <- substring(xx, 1, nchar(xx) - 4)
  return(xy)
}



#' #Extract all the mutants in a folder
#'
#' @param chem1 the directory path
#' @return a list of mutants names for the specified directory path 
getmutants <- function(chem1)
{
  tp <- list.files(chem1)
  return(lapply(tp, extractor))
}


#' Extract all mutants in all folders
#'
#' @param folders all the the directories paths
#' @return a list of lists containing the mutants names for each directory path 
getAllMutants <- function(folders)
{
  allMut <- lapply(folders, getmutants)
  return(allMut)
}


######## Formatting and getting all the information in a suitable format!! #######

#' Format a list of dataframes using the mini_formatter function 
#'
#' @param list2DF a list of dataframes
#' @return a list of formatted dataframes
Formatter <- function(list2DF,noms)
{
  lapply(list2DF, mini_formatter, noms2 = noms)
}

#' Format one dataframe, in order to get a structure suitable for the IsolatorPlotter function.
#'
#' @param df a dataframe
#' @return the formatted dataframe
mini_formatter <- function(df,noms2){
  # Select the first rows of the DF (until the labels containing row)
  df1 <- df[1:which(df == noms2[1])-1,]
  df1 <- df[with(df1, order(df1[,1], df1[,2])),]
  
  # Select the last rows of the DF (after the labels containing row)
  df2 <- na.omit(df[which(df == noms2[1])+1:dim(df)[1],])
  df2 <- df2[with(df2, order(df2[,1], df2[,2])),]
  
  # Convert all characters into numerics and label the columns
  colnames(df1) <- noms2
  df1 <- as.data.frame(lapply(df1, function(x) as.numeric(as.character(x))))
  
  
  # Attractor info
  behaviors <- paste0(noms2, ".behavior")
  colnames(df2) <- behaviors
  df2_sub <- df2[,3:length(df2)]
  # Replace numeric values by the hyphen character, which means the value is stable inside the attractor
  df2_sub <- data.frame(lapply(df2_sub, function(x){
    gsub("[0-9]","-", x)
  }))
  
  #Define the attractor type depending of oscillating behaviors
  osc_or_not <- apply(df2_sub,1,function(u) grepl("\\~",u))
  attractor_type <- apply(osc_or_not, FUN = function(x){ if(any(x)){ return("O") } else { return("SS") }}, 2 )
  df2_sub$Attractor_type <- attractor_type
  
  # Bind the two tables (numeric results + attractor info)
  fulltable <- cbind(df1, df2_sub)
  return(fulltable)
}



#' Apply the Formatter function to all folders
#'
#' @param Bigstuff the nested list of raw dataframes 
#' @return the list containing the list of formatted dataframes
G_Formatter <- function(Bigstuff,nomsx)
{
  #print("Formatting in progress...")
  return(lapply(Bigstuff, Formatter, noms = nomsx))
  #print("Formatting = DONE")
}


########### ISOLATOR PART ################


#' Comparison between 2 values
#'
#' @param number a number
#' @param interval a vector
#' @return boolean indicating whether the number is between the values in the interval
checkInterval2 <- function(number, interval) 
{
    numb <- as.numeric(number)
    return(numb > interval[1] && numb < interval[2])
}


#
# 

#' Subset data to get the desired output.
#' The final table = The two inputs + outputvalue + output dynamics (oscillations or steady state)
#' @param Tableau a dataframe (formatted)
#' @param inputlist the list of model inputs (length = 2 for this purpose)
#' @param singleoutput one of the outputs of interest
#' @return a dataframe in which the output is isolated, with the information for each input couple about its value and attractor dynamics
Subsetter <- function(Tableau,inputlist,singleoutput)
{

  if (length(grep(singleoutput,colnames(Tableau)))> 1)
  {
    #Search the column with the node only
    O1 <- which(colnames(Tableau) %in% singleoutput)
    # Search the column with the node + Behavior
    O2 <- grep("behavior", colnames(Tableau),value = T)
    if (length(grep(singleoutput,O2, value = T))>1) # If there are similar names:
    {
      O2.5 <- grep(singleoutput,O2, value = T)
      lister <- substring(O2.5, 1, regexpr("\\.", O2.5) - 1)
      O3 <- O2.5[which(lister %in% singleoutput)]
    } else { # If there is only 1 name :
      O3 <- grep(singleoutput,O2, value = T)
    }
   
    #Identification
    O4 <- grep(O3, colnames(Tableau))
    O <- c(O1,O4)
    #print(colnames(Tableau)[O])
  }
  
  else 
  {
    O <- grep(singleoutput,colnames(Tableau))  
  }
  #Same goes for the input
  
  I <- lapply(inputlist, MultipleGrepNoOverlap, colonnes = colnames(Tableau))
  IO <- unique(unlist(c(I,O)))
  # Isolate the output desired with the inputs info
  return(subset(Tableau, select = IO))
}

### 
#' Multiple Grep with selection for possible overlapping matchs (works only for inputs)...
#' @param input one input
#' @param colonnes the colnames
#' @return the result of the grep search for the input in the colnames, without any overlap
MultipleGrepNoOverlap <- function(input,colonnes)
{
  if(length(grep(input,colonnes))> 1 )
  {
    return(which(colonnes %in% input))
  } else 
  {
    return(grep(input,colonnes))
  }
}


#' Transforms into discrete values the output table values
#' For boolean values = OFF / ON. For multivalued ones = Low, Mid, High
#' @param Table a dataframe (isolated)
#' @param outputname the output of interest
#' @return the Table with the information of the discretisation.
discretisator <- function(Table,outputname)
{
  #print(Table)
  beh <- grep("behavior",colnames(Table))
  bo <- grep(outputname,colnames(Table))
  OOF <- bo[which(!bo %in% beh)]
  OOJ <- bo[which(bo %in% beh)]
  e <- Table[,OOF]
  j <- Table[,OOJ]
  nomdetype <- paste(outputname,"type", sep = "_")
  if (any(grepl(outputname, x = MULTIVALUED)))
  {
    # Low / Medium / High attribution
    ox <- as.factor(unlist(lapply(e, discretisation_multi, valeursmulti = VALUESMULTI)))
    Table$X <- ox
    i <- grep("X",colnames(Table), fixed = T)
    colnames(Table)[i] <- nomdetype
    return(Table)
  }
  else
  {
    # ON / OFF attribution
    ox <- as.factor(unlist(lapply(e, discretisation_bool)))
    Table$X <- ox
    i <- grep("X",colnames(Table), fixed = T)
    colnames(Table)[i] <- nomdetype
    return(Table)
  }
}


#' Attribution of discrete values for multivalued nodes.
#' @param x a value
#' @param Valeursmulti a vector = {0,1,2}, hardcoded
#' @return The character "Low", "Medium", or "High". 
discretisation_multi <- function(x, valeursmulti) #
{
  n <- as.numeric(x)
  #print(valeursmulti)
  if (n == valeursmulti[1])
  {
    return("Low")
  }
  else if (n > valeursmulti [1] && n <= valeursmulti[2])
  {
    return("Medium")
  }
  else
  {
    return("High")
  }

}

#' Attribution of discrete values for boolean nodes.
#' @param x a value
#' @return The character "ON", or "OFF".
discretisation_bool <- function(x)
{
  n <- as.numeric(x)
  if (n == 0)
  {
    return("OFF")
  }
  else
  {
    return("ON")
  }
}

# 
#' Isolate and inserts discrete values for the desired outputs
#' @param df a dataframe
#' @param i the inputs
#' @param o the desired output
#' @return The isolated and discretized output for the dataframe (corresponding to a mutant) 
Isolator <- function(df,i,o)
{
  #print(o)
  S1 <- Subsetter(df,i,o)
  S2 <- discretisator(S1,o)
  return(S2)
}

#TestIsolator 1color scale continuous 4 plotting
DarkIsolator <- function(df,i,o)
{
    #print("Isolation en cours")
    S1 <- Subsetter(df,i,o)
    return(S1)
}

######### PLOTTER PART #########



#1 color, several shades

#' Generates the Heatmap for one desired output. Handles the bistability by separating a tile in several pieces (for the moment two)
#' @param Table a dataframe
#' @param i the inputs
#' @return A heatmap with one color, but several shades.
Darkplotter <- function(Table, i)
{

  #First, prepare the dataframe to deal with bistability
  e01 <- data.table(Table)

  # We insert shifts in order to deal with bistability. If one couple of input allows bistability, then we separate the tile in 2 or more (depends of the number of attractors in that condition)
  e2 <- e01[,shift:=(1:(.N))/.N - 1/(2 * .N) - 1/2, by=eval(colnames(e01)[1:2])] 
  #print(e2)
  e2[, height:=1/.N, eval(colnames(e01)[1:2])]

  e2[,3] <- as.numeric(unlist(e2[,3]))

  y <- colnames(e2)[3]
  e3 <- data.frame(e2)

  
  # Now, let's plot everything!
  if (any(grepl(y,BOOLEAN)))
         {
           couleur <- DARK_COLORSCALES[[1]]

           behave <- grep("behavior", colnames(Table))

           Arupha <- ggplot(data = e3, aes(x = e3[,2], y = e3[,1] + shift, height = height)) + theme_minimal() + geom_tile(color="black",size=2,aes(fill = e3[,y]))+ scale_colour_gradient2() +  geom_text(aes(label = e3[,behave]), size = 10) + scale_y_reverse() + scale_x_discrete(position = "top")
           
           Arupha <- Arupha + scale_fill_continuous("value",low = COLORSCALE_BOOL[[1]],high= COLORSCALE_BOOL[[2]], limits = c(0,1), breaks = c(0,0.5,1)) + theme(plot.caption= element_text(hjust = 0.5, size = 20)) + labs(caption =y) + ylab(colnames(e3)[1]) + xlab(colnames(e3)[2])

         }
  else
          #Multivalued!
         {
           couleur <- DARK_COLORSCALES[[2]]
           behave <- grep("behavior", colnames(Table))
#cheatcode : third col = the good one
           Arupha <- ggplot(data = e3, aes(x = e3[,2], y = e3[,1] + shift, height = height)) + theme_minimal() + geom_tile(color="black",size=2,aes(fill = e3[,y]))+ scale_colour_gradient2() +  geom_text(aes(label = e3[,behave]), size = 10)+ scale_y_reverse() + scale_x_discrete(position = "top")
           Arupha <- Arupha + scale_fill_continuous("value",low = COLORSCALE_MULTI[[1]],high= COLORSCALE_MULTI[[3]], limits = c(0,2)) + theme(plot.caption= element_text(hjust = 0.5, size = 20)) + labs(caption =y) + ylab(colnames(e3)[1]) + xlab(colnames(e3)[2])
         }
  return(Arupha)
}



# From formatted simulation results, get all plots for the studied outputs
IsolatorPlotter <- function(SimulationResultsTable, inp,t, v) #i = INPUTS, o = OUTPUTS, t = THEOUTPUTS, v = Multivalued values
{
  #Then, we isolate the outputs in different tables, and we discretize the node value (double -> Qualitative)
  b <- lapply(t, Isolator, i = inp, df = SimulationResultsTable)
  #And we plot each output
  c <- lapply(b, plotter)
  return(c)
}


#' # From one formatted simulation table, isolate by output and get all Heatmaps for the studied outputs
#' @param SimulationResultsTable fr
#' @param inp the inputs
#' @param t the desired outputs
#' @param v the multivalued values (hardcoded for the moment)
#' @return A list of plots for one mutant, and each output.
DarkIsolatorPlotter <- function(SimulationResultsTable, inp,t, v) 
{
  #print("Isolation en cours")
  # print(THEOUTPUTS)
  # print(typeof(THEOUTPUTS))
  #Then, we isolate the outputs in different tables, and we discretize the node value (double -> Qualitative)
  b <- lapply(t, Isolator, i = inp, df = SimulationResultsTable)
  #And we plot each output
  #print(b)
  c <- lapply(b, Darkplotter, i = inp)
  return(c)
}

###### PRINT BIG SCALE PART #######


#' Prints plots for all mutants (1 pdf = 1 list mutants)
#' Scalable depending of ram usage. If you have enough ram and cores, use mcmapply.
#' @param WholePlotList the nested list of all plots from the DarkMegaIsolatorPlotter function
#' @param WholeMutantList the list of mutants for all folders
#' @param folderlist the list of folders
#' Generates all pdf files for each folder which contain all mutants (and all associated outputs) for each pdf file.
ULTIMATE_PRINTER <- function(WholePlotList,WholeMutantList,folderlist)
{
  noms <- gsub("\\.//", "", folderlist)
  N_A_M_E_S <- paste(noms,"_Readouts.pdf",sep = "")
  mapply(ThePlotPrinter, 
         Megalist2plots = WholePlotList, 
         list2mutants =WholeMutantList, 
         plotname = N_A_M_E_S)
}

### For IS mutants only
#' Prints plots for all mutants of Interest (1 pdf = 1 list mutants)
#' Scalable depending of ram usage. If you have enough ram and cores, use mcmapply.
#' @param Allplots the nested list of all plots from the DarkMegaIsolatorPlotter_IS function
#' @param Mutants_detected the list of mutants of interest (with the names & specific folders)
#' Generates all pdf files for each folder which contain all mutants (and all associated outputs) for each pdf file.

ULTIMATE_PRINTER_IS <- function(Allplots, Mutants_detected) 
{
  noms <- gsub("\\.//", "", Mutants_detected[[3]])
  N_A_M_E_S <- paste(noms,"_IS_mutants.pdf",sep = "")
  mapply(ThePlotPrinter, 
         Megalist2plots = Allplots, 
         list2mutants =Mutants_detected[[2]], 
         plotname = N_A_M_E_S)
}



ThePlotPrinter <- function(Megalist2plots,list2mutants,plotname)
{

  # Evaluates the number of outputs in the plotlists
  n_cols = length(Megalist2plots[[1]])
  if (n_cols <= 2)
  {
    pdf(plotname, width = (4.5 * n_cols), height = (1.5 * (n_cols)))
    mapply(FUN = PlotPrinter, list2plots = Megalist2plots, mutant = list2mutants, n_col = n_cols)
    dev.off()  
  } else
  {
    pdf(plotname, width = (4.5 * n_cols), height = (1.5 * (n_cols)))
    mapply(FUN = PlotPrinter, list2plots = Megalist2plots, mutant = list2mutants, n_col = n_cols)
    
    dev.off() 
  }
  
}

#' Arrange the plots on one page, with a scalable size depending of the number of outputs.
#' @param list2plots the list of plots from the DarkIsolatorPlotter function
#' @param mutant the mutant studied
#' @param n_col the number of columns, a.k.a the number of outputs studied (essential to determine the page size)
#' @return a grid containing all the plots with the mutant name on the left of the grid.
PlotPrinter <- function(list2plots,mutant, n_col)
{
  #plot_grid(plotlist = Might, ncol = 5, )
  do.call("grid.arrange", list(grobs = list2plots, ncol= 4, left = textGrob(mutant, gp=gpar(fontsize=15,font=8), rot = 90)))
}



#### Detection No Isc No Suf (2 outputs only).
Detection.No.IS <- function(dataframe,output1, output2)
{
  #  dataframe <- okk
  #   output1 <- "IscSUA"
  #    output2 <- "Suf"
  o1 <- dataframe[,grep(output1, colnames(dataframe))]
  o2 <- dataframe[,grep(output2, colnames(dataframe))]
  bool <- mapply(comparison,o1,o2)
  return(any(bool))
}


#' Detection of interesting mutants for one dataframe
#' @param df the dataframe
#' @param listofoutputs the list of outputs
#' @return a boolean value indicating if there is at least one condition satisfying the condition from the user (currently all values = 0).
Detection_NO_IS_generalized <- function(df, listofoutputs)
{
  to_compare <- lapply(listofoutputs, function(x){
    return(df[,which(colnames(df) %in% x)])
  })
  # Compares all vectors 1 by 1 to check if everything is equal 0.
  # the do.call allows me to pass the elements of "to_compare" as arguments
  bool <- do.call(mapply, c(list(FUN = comparison_gen), to_compare))
  return(any(bool))
}

#Compare for 2 values
comparison <- function(a,b){return(a == 0 && b == 0)}

#' Comparison for n values
#' @param ... the first value of each list
#' @return a boolean value indicating if all values = 0 or not.
comparison_gen <- function(...){
  x <- c(...)
  #print(x)
  return(all(x %in% 0))
}

#Goal = obtain a boolean list where two outputs are at 0, the index is used to get the mutants list fulfilling that condition

#For 2 outputs
MultiDetection.No.IS <- function(listDF, out1,out2)
{
  X <- lapply(X = listDF, FUN = Detection.No.IS, output1 = out1, output2 = out2)
  return(X)
}

#For n outputs
#' Detection of interesting mutants, for a list of dataframes.
#' @param listDF the list of dataframes (for 1 folder)
#' @param outputlist the list of outputs
#' @return a list of booleans indicating that one mutant has at least 1 condition where the comparison is satisfied
MultiDetection.No.IS_gen <- function(listDF, outputlist)
{
  X <- lapply(X = listDF, FUN = Detection_NO_IS_generalized, listofoutputs = outputlist)
  return(X)
}

#' Get all the mutants of interest filenames
#' @param listOfFiles the file list
#' @param MutIS the list of mutants found
#' @return the list of filenames

get.IS.filenames <- function(listOfFiles, MutIS)
{
  x <- lapply(MutIS, grep, x = listOfFiles, value = T)
  return(x)
}

### Deprecated functions ----

MegaIsolatorPlotter <- function(allSimulations,folders)
{
  Omega <- mapply(UltimateIsolatorPlotter, list2simulations = allSimulations, Folder = folders)
  return(Omega)
}

UltimateIsolatorPlotter <- function(list2simulations, Folder)
{
  AllPlots4mutants <- lapply(list2simulations, IsolatorPlotter, inp = INPUTS, o = OUTPUTS, t = THEOUTPUTS, v = VALUESMULTI)
  allmutNames <- getmutants(Folder)
  names(AllPlots4mutants) <- mutnames
  return(AllPlots4mutants)
}

# Deprecated, but if you want to use 5 different colors feel free.
plotter <- function(Table)
{
  
  y <- colnames(Table)[3]
  e <- grep(y,THEOUTPUTS)
  switch(e,
         {
           couleur <- COLORSCALES[[1]]
           behave <- grep("behavior", colnames(Table))
           type <- grep("type", colnames(Table))
           Arupha <- ggplot(data = Table, aes(x = i[2], y = i[1])) + theme_minimal() + geom_tile(aes(fill = Table[,dim(Table)[2]])) +  geom_text(aes(label = Table[,behave]), size = 10)+ scale_y_reverse() + scale_x_discrete(position = "top")
           Arupha <- Arupha + scale_fill_manual("",values = c("Low"=couleur[[1]],"Medium"=couleur[[2]], "High"=couleur[[3]] )) + theme(plot.caption= element_text(hjust = 0.5, size = 20)) + labs(caption =y)
         },
         {
           couleur <- COLORSCALES[[2]]
           #print(couleur[[1]])
           behave <- grep("behavior", colnames(Table))
           type <- grep("type", colnames(Table))
           Arupha <- ggplot(data = Table, aes(x = i[2], y = i[1])) + theme_minimal() + geom_tile(aes(fill = Table[,type])) +  geom_text(aes(label = Table[,behave]), size = 10)+ scale_y_reverse() + scale_x_discrete(position = "top")
           Arupha <- Arupha + scale_fill_manual("",values = c("OFF"=couleur[[1]],"ON"=couleur[[2]]))  + theme(plot.caption= element_text(hjust = 0.5, size = 20)) + labs(caption =y)
           return(Arupha)
         },
         {
           couleur <- COLORSCALES[[3]]
           #print(output)
           #print(couleur)
           behave <- grep("behavior", colnames(Table))
           type <- grep("type", colnames(Table))
           Arupha <- ggplot(data = Table, aes(x = i[2], y = i[1])) + theme_minimal() + geom_tile(aes(fill = Table[,type])) +  geom_text(aes(label = Table[,behave]), size = 10)+ scale_y_reverse() + scale_x_discrete(position = "top")
           Arupha <- Arupha + scale_fill_manual("",values = c("OFF"=couleur[[1]],"ON"=couleur[[2]]))  + theme(plot.caption= element_text(hjust = 0.5, size = 20)) + labs(caption =y)
         },
         {
           couleur <- COLORSCALES[[4]]
           behave <- grep("behavior", colnames(Table))
           type <- grep("type", colnames(Table))
           Arupha <- ggplot(data = Table, aes(x = i[2], y = i[1])) + theme_minimal() + geom_tile(aes(fill = Table[,dim(Table)[2]]))  +  geom_text(aes(label = Table[,behave]), size = 10)+ scale_y_reverse()  + scale_x_discrete(position = "top")
           Arupha <- Arupha + scale_fill_manual("",values = c("Low"=couleur[[1]],"Medium"=couleur[[2]], "High"=couleur[[3]] )) + theme(plot.caption= element_text(hjust = 0.5, size = 20)) + labs(caption =y)
         },
         {
           couleur <- COLORSCALES[[5]]
           behave <- grep("behavior", colnames(Table))
           type <- grep("type", colnames(Table))
           Arupha <- ggplot(data = Table, aes(x = i[2], y = i[1])) + theme_minimal() + geom_tile(aes(fill = Table[,type])) + geom_text(aes(label = Table[,behave]), size = 10)+ scale_y_reverse() + scale_x_discrete(position = "top")
           Arupha <- Arupha + scale_fill_manual("",values = c("OFF"=couleur[[1]],"ON"=couleur[[2]]))+ theme(plot.caption= element_text(hjust = 0.5, size = 20)) + labs(caption =y)
         }
  )
  return(Arupha)
}
