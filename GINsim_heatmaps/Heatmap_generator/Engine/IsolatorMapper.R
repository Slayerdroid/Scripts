#!/usr/bin/env Rscript

##### All the things required to run the IsolatorMapper script -----

#For manual purpose in local...
# base <- "~/Bureau/Prototype data analysis/Tools/Scripts/GINsim_heatmaps/Heatmap_generator/Engine/"
# setwd(base)

## Get the required packages
# ipkgs <- function(pkg){
#   new.pkg <- pkg[!(pkg %in% installed.packages()[, "Package"])]
#   if (length(new.pkg))
#     install.packages(new.pkg, dependencies = TRUE)
#   sapply(pkg, require, character.only = TRUE)
# }

# ipkgs <- function(pkg){
#   lapply(pkg, load.fun)
# }


# load.fun <- function(x) {
#   #x <- as.character(substitute(x))
#   print(x)
#   if(isTRUE(x %in% .packages(all.available=TRUE))) {
#     eval(parse(text=paste("require(", x, ")", sep="")))
#   } else {
#     eval(parse(text=paste("install.packages('", x, "', repos = 'http://cran.us.r-project.org')", sep="")))
#     eval(parse(text=paste("require(", x, ")", sep="")))
#   }
# } 

library(here)
library(data.table)
library(ggplot2)
library(tidyr)
library(parallel)
library(Matrix)
library(gridExtra)
library(reshape2)
#library(reshape)
library(RColorBrewer)
library(ggpubr)
#library(cowplot)
library(grid)
#library(argparse)
#library(argparser)


#Load existing packages, install missing packages [DEPRECATED, the conda environment does the job]
#required.packages <- c('here','data.table','ggplot2','tidyr','reshape','parallel','Matrix', 'gridExtra','reshape2','RColorBrewer','ggpubr','cowplot','grid','argparse','argparser')
# ipkgs(required.packages)
#load.fun(required.packages)

# here()
# print(getwd())




#Script mode Arguments Parser ----
# p <- arg_parser("A script making heatmaps based on GINsim results")
# 
# 
# # specify our desired options
# 
# p <- add_argument(p, "-n", "--modelNodes", help = "Model nodes text file", flag = F)
# p <- add_argument(p, "-i", "--inputs", help = "Input nodes text file (needs 2 inputs at least & at most)", flag = F)
# p <- add_argument(p,"-b", "--booleanNodes", help = "Boolean nodes text file", flag = F)
# p <- add_argument(p, "-M", "--multivaluedNodes", help = "Multivalued nodes text file", flag = F)
# p <- add_argument(p, "-r", "--readouts", help = "Model readout nodes text file (the nodes we are interested in", flag = F)
# p <- add_argument(p, "-o", "--outputs", help = "Model output nodes text file (the nodes we want to display in the pdf file)", flag = F)
# p <- add_argument(p, "-ISo", "--IS_outputs", help = "Model output nodes, specific to Isc and Suf text file (displayed in the pdf file)", flag = F)
# 
# 
# required_arguments <- c(4:10)
# 
# #Getting all the inputfiles in a parser
# arguments <- commandArgs(FALSE)
# true_args <- arguments[c(7:length(arguments))]
# 
# #Verify whether there are no missing arguments
# if (length(true_args)-6 != 7)
#   {
#     print(p)
#     print("Usage: \n Rscript IsolatorMapper.R -n Model.txt -i Inputs.txt -b Boolean.txt -M Multivalued.txt -r Readouts.txt -o TheOutputs.txt -ISo IS_outputs.txt")
#     quit(save = "no")
#   } else 
#   
#   {
#     argz <- parse_args(p)
#     True_arguments <- argz[required_arguments]
    # source("IsolatorHelper.R")
#   }


ptm <- proc.time()










#GLOBAL VARIABLES ----

# Uncomment for manual diagnostic
# NAMES <- c("Fe_ext","O2","Fe_free","Fur","RyhB","H2O2","OxyR","Hpx","Suf","IscR_A","IscR_H","Isc","ErpA","NfuA")
# INPUTS <- c("Fe_ext","O2")
# BOOLEAN <- c("H2O2","ErpA","NfuA","Isc","IscR_A","IscR_H","OxyR")
# MULTIVALUED <- c("Fe_ext","O2","Hpx","Fe_free","Fur","RyhB","Suf")
# OUTPUTS <- c("Fe_free","H2O2","Isc","ErpA") #For the Labeller function
# THEOUTPUTS <- c("Fe_free","H2O2","Isc","Suf", "ErpA") #For the plotting part
# THEOUTPUTS <- c("Fe_free","Isc","Suf")
# IS.OUTPUTS <- c("Isc","Suf")


#Automatic setting
# NAMES <- readLines(True_arguments[[1]])
# INPUTS <- readLines(True_arguments[[2]])
# BOOLEAN <- readLines(True_arguments[[3]])
# MULTIVALUED <- readLines(True_arguments[[4]])
# OUTPUTS <- readLines(True_arguments[[5]])
# THEOUTPUTS <- readLines(True_arguments[[6]])
# IS.OUTPUTS <- readLines(True_arguments[[7]])




print(THEOUTPUTS)

### Do not change yet...
# VALUESFUR <- c(1,2)
# VALUESMULTI <- c(0,1,2)
# VALUESBOOL <- c(0,1)
# 
# ## Colorscales
# COLORSCALE_MULTI <- c("#ffffff","#c8afff", "#cc00ff")
# COLORSCALE_BOOL <- c("#ffffff","#cc00ff")
# 
# 
# DARK_COLORSCALES <- list(COLORSCALE_BOOL,COLORSCALE_MULTI)




#### ISOLATOR MAPPER ####

# PHASE I: INTEGRATE EACH MUTANT ONE BY ONE IN A LIST OF DATAFRAMES.



#Get directories
# Folderz <- list.dirs("./")
# Folders <- grep(pattern = "List",x = Folderz, value = T)

# Foldnull <- grepl(pattern = "LOL", x = Folders)
# any(Foldnull)
# any(Folders_present)

# Folders <- Folderz[2:length(Folderz)]
# #Filess <- list.files()
# 
# Folders_present <- grepl(pattern = "List",x = Folders)
# 

#Check if there are not more directories than the number of mutant list files
# if (length(Folders) != length(List_combinaisions_reel))
# {
#   print("There are directories non-related to the mutant files, please delete them before launching this script again.")
#   quit(save = "no")
# }
# 
# if (!any(Folders_present))
# {
#   print("No folder containing the GINsim results. Please run the GINsim simulation step before generating the heatmaps.")
#   quit(save = "no")
# }


### Get mutants + format to get a useful structure

#Get all Mutants names
# allmutNames <- getAllMutants(Folders)



#Get all tables from directories
# ALLMUT <- lapply(Folders, reader)

# Format every table obtained before ^
# ALLMUT <- G_Formatter(ALLMUT,NAMES)



#### Diagonosis For the Formatting part ----
#On sépare la liste de mutants en 3 (simple, 2x, 3x)
# TEST00 <- ALLMUT[[1]]
# TestIS <- TEST00[[7]]

## Isc vs WT
# TestWT <- ALLMUT[[1]][[4]]
# TeestIscRAKO <- ALLMUT[[1]][[2]]

# Test001 <- ALLMUT[[1]]
# Stab1 <- Test001[[length(Test001)]]
# Bistab <- TEST00[[49]]

#ee <- mini_formatter(Stab1,NAMES)
# 
# Formator <- Formatter2(Test001,NAMES)

## Diagnosis for the isolator part ----
# WT <- Formator[[length(Formator)]]
# WT_subisc <- Subsetter(TestWT,INPUTS,"Isc")
# IscRAKO_subisc <- Subsetter(TeestIscRAKO,INPUTS,"Isc")
# xy <- Darkplotter(WT_sub, INPUTS)
# xy


#xx <-lapply(THEOUTPUTS, Isolator, i = INPUTS, df = abb)
#xy <- Darkplotter(xx[[1]])
#e <- xx[[1]]
#Darkplotter(xx[[1]])


#TEST10 <- ALLMUT[[1]]
#Test20 <- ALLMUT[[3]]
# Testx0 <- TEST00[[1]]
# abcd <- L_abeller(Testx0,OUTPUTS,VALUESMULTI)
# xx <-lapply(THEOUTPUTS, Isolator, i = INPUTS, df = a)
# xy <- Darkplotter(xx[[3]])
# 
# ZZ <- DarkIsolatorPlotter(TEST00[[1]],INPUTS,THEOUTPUTS,VALUESMULTI)

# Diagnosis for the mutants searching engine ----

#Get all mutants names containing Isc or Suf= 0

# IS.OUTPUTS_multi <- c("Isc","Suf","ErpA")
# No_Isc_no_Suf <- lapply(ALLMUT, MultiDetection.No.IS ,out1 = "Isc", out2 = "Suf")

##### Search for mutants [IS function] ####
#' Detection of interesting mutants, to get the interesting mutants dataset.
#' @param all_perturbs the nested list of all the perturbations
#' @param all_results the whole formatted dataset
#' @param model_outputs the list of outputs the user wants to check
#' @param model_nodes the list of model nodes
#' @param the_folders the folders list
#' @return the dataset containing the mutants of interest (in which the model_outputs are all at 0 for at least one condition)

Search4Mutants <- function(all_perturbs, all_Results, model_outputs, model_nodes, the_folders)
{
  mutants_detected <- lapply(all_Results,
                             MultiDetection.No.IS_gen ,
                             outputlist = model_outputs)
  List_of_mutants_detected <- mapply(
    function(partMutNames,is_mutant){
      return(partMutNames[which(is_mutant == T)])
      }, 
    partMutNames = all_perturbs, 
    is_mutant = mutants_detected)
  
  # Then, check whether there is an empty list
  Folders_IS <- Folders
  index <- c()
  for (i in 1:length(List_of_mutants_detected))
  {
    if (length(List_of_mutants_detected[[i]]) == 0)
    {
      index[i] <- i
    }
  }
  
  if (length(index)>0) #If there is 1+ Folders where no mutant was detected:
  {
    # Modify the file lists since there are folders without the mutants of interest
    List_of_mutants_detected <- List_of_mutants_detected[-index]
    Folders_IS <- Folders[-index]
    # Get all tables associated to mutants
    ALL.IS.MUTmult <- mapply(reader2,
                             chem1 = Folders_IS,
                             MutIS =  List_of_mutants_detected)
    ALL.IS.MUTmult <- G_Formatter(ALL.IS.MUTmult,NAMES)
    return(list(ALL.IS.MUTmult, 
                List_of_mutants_detected, 
                Folders_IS))
    
  } else {
    # Get all tables associated to mutants
    ALL.IS.MUTmult <- mapply(reader2, 
                             chem1 = Folders_IS, 
                             MutIS =  List_of_mutants_detected)
    ALL.IS.MUTmult <- G_Formatter(ALL.IS.MUTmult,NAMES)
    return(list(ALL.IS.MUTmult,
                List_of_mutants_detected, 
                Folders_IS))
  }
  
  
}

# Mutants_obtained <- Search4Mutants(allmutNames,ALLMUT, IS.OUTPUTS_multi, NAMES, Folders)


### Diagnosis Isolator plotter -----
# TestIS_plot <- Mutants_obtained[[1]][[1]][[1]]
# ZZ <- DarkIsolatorPlotter(TestIS_plot,INPUTS,THEOUTPUTS,VALUESMULTI)
# ZZ
# Test_Plot <- ALLMUT[[1]][[length(ALLMUT[[1]])]]
# ZZ <- DarkIsolatorPlotter(Test_Plot,INPUTS,THEOUTPUTS,VALUESMULTI)
# ZZ

# Functions for large scale Isolator plotter ----

#

#' For all mutants of a folder, get all the plots for each output you wanted
#' @param list2simulations the list of formatted dataframes for one folder
#' @param MutantList the list of mutants for one folder
#' @param i the list of inputs
#' @param o the list of outputs
#' @return a list containing a list of plots (for each output).
DarkUltimateIsolatorPlotter <- function(list2simulations, MutantList, i, o)
{
  AllPlots4mutants <- lapply(list2simulations, DarkIsolatorPlotter, inp = i, t = o, v = VALUESMULTI)
  names(AllPlots4mutants) <- MutantList
  return(AllPlots4mutants)
}


#' For all mutants of a folder, get all the plots for each output you wanted (for mutants of interest)
#' @param list2simulations the list of formatted dataframes for one folder
#' @param MutantList the list of mutants for one folder
#' @param i the list of inputs
#' @param o the list of outputs
#' @return a list containing a list of plots (for each output).
DarkUltimateIsolatorPlotter_IS <- function(list2simulations, MutantISList, i, o)
{
  AllPlots4mutants <- lapply(list2simulations, DarkIsolatorPlotter, inp = i, t = o, v = VALUESMULTI)
  names(AllPlots4mutants) <- MutantISList
  return(AllPlots4mutants)
}





#' For all mutants of all folders, get all the plots for each mutant and output you wanted.
#' @section NB :
#' Scalable depending of ram usage. If you have enough ram and cores, use mcmapply.
#' @param allSimulations the nested list of all dataframes
#' @param allMutantList the list of mutants for all folders
#' @param In the list of inputs
#' @param Out the list of outputs
#' @return a nested list containing a list of plots (for each output).
DarkMegaIsolatorPlotter <- function(allSimulations,allMutantList, In , Out)
{
  Omega <- mapply(DarkUltimateIsolatorPlotter, 
                  list2simulations = allSimulations, 
                  MutantList = allMutantList, 
                  MoreArgs = list(i = In, o = Out))
  return(Omega)
}


#' For all mutants of all folders, get all the plots for each mutant and output you wanted. For mutants of interest.
#' @section NB :
#' Scalable depending of ram usage. If you have enough ram and cores, use mcmapply.
#' @param allSimulations the nested list of all dataframes
#' @param allMutantList the list of mutants for all folders
#' @param In the list of inputs
#' @param Out the list of outputs
#' @return a nested list containing a list of plots (for each output).
DarkMegaIsolatorPlotter_IS <- function(allSimulations,allMutantList, In, Out)
{
  Omega <- mcmapply(DarkUltimateIsolatorPlotter_IS, 
                  list2simulations = allSimulations, 
                  MutantISList = allMutantList,
                  MoreArgs = list(i = In, o = Out),
                  mc.cores = 3)
  return(Omega)
}


# Omegamon = Digimon name, but actually it's the plot list for each mutant and each outputs used 
# Omegamon <- DarkMegaIsolatorPlotter_IS(Mutants_obtained[[1]],Mutants_obtained[[2]], INPUTS, IS.OUTPUTS_multi)

# Omegamon.merciful <- DarkMegaIsolatorPlotter(ALLMUT,allmutNames, INPUTS, THEOUTPUTS)


#### Printing part ----



#' Prints plots for all mutants (1 pdf = 1 list mutants)
#' Scalable depending of ram usage. If you have enough ram and cores, use mcmapply.
#' @param WholePlotList the nested list of all plots from the DarkMegaIsolatorPlotter function
#' @param WholeMutantList the list of mutants for all folders
#' @param folderlist the list of folders
#' Generates all pdf files for each folder which contain all mutants (and all associated outputs) for each pdf file.
ULTIMATE_PRINTER <- function(WholePlotList,WholeMutantList,folderlist)
{
  noms <- gsub("\\.//", "", folderlist)
  print(noms)
  N_A_M_E_S <- paste(noms,"_Readouts.pdf",sep = "")
  mapply(ThePlotPrinter, 
         Megalist2plots = WholePlotList, 
         list2mutants =WholeMutantList, 
         plotname = N_A_M_E_S)
}




### For IS mutants only
#' Prints plots for all mutants of Interest (1 pdf = 1 list mutants)
#' Scalable depending of ram usage. If you have enough ram and cores, use mcmapply.
#' @param Allplots the nested list of all plots from the DarkMegaIsolatorPlotter_IS function
#' @param Mutants_detected the list of mutants of interest (with the names & specific folders)
#' Generates all pdf files for each folder which contain all mutants (and all associated outputs) for each pdf file.

ULTIMATE_PRINTER_IS <- function(Allplots, Mutants_detected) 
{
  noms <- gsub("\\.//", "", Mutants_detected[[3]])
  #print(noms)
  N_A_M_E_S <- paste(noms,"_IS_mutants.pdf",sep = "")
  mapply(ThePlotPrinter, 
         Megalist2plots = Allplots, 
         list2mutants =Mutants_detected[[2]], 
         plotname = N_A_M_E_S)
}



# ThePlotPrinter(Omegamon.merciful,allmutNames, "Test.pdf")

#ULTIMATE_PRINTER(Omegamon.merciful,allmutNames,Folders)
# 
# ULTIMATE_PRINTER_IS(Omegamon, Mutants_obtained)

proc.time() - ptm



