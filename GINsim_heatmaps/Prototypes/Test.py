from org.ginsim.common.callable import BasicProgressListener
from org.ginsim.common.callable import BasicProgressListener
from org.ginsim.core.graph.regulatorygraph.perturbation import PerturbationFixed
from org.ginsim.core.graph.regulatorygraph.perturbation import PerturbationMultiple
from org.ginsim.service.export.nusmv import NuSMVConfig
from org.ginsim.core.graph.regulatorygraph.perturbation import PerturbationRegulator
import os, sys
import re
import getopt
import imp

import csv
import itertools

#import simplejson


print(sys.version)
if len(gs.args)==0:
    print("Usage: java -jar PATH/GINsim.jar -s attractors.py model.zginml -p perturbations.txt -r")
    sys.exit(1)

if gs.args[0].find(".zginml")<0:
    print("The first argument should be a GINsim file (.zginml)")
    sys.exit(1)

print(gs.args[0])

g = gs.open(gs.args[0])
fmodel=gs.args[0]
reg=re.findall("([\w\-\_]*)",fmodel)
if(len(reg)<4):
   modelname=fmodel
else:
    modelname=reg[-4]
print("########################")
print("Model "+fmodel)
print("########################")

if not os.path.isfile(fmodel):
    print(fmodel+" file not found")
    sys.exit (1)

fperturb=''
freport=''




# Boolean nodes getter (not input)
def getInternalBooleanNodes(nodes):
    boolean_nodes = []
    for node in nodes:
        if node[1] == 1 and not node[2]:
            print(node[0])
            boolean_nodes += [(str(node[0]), node[1])]
    return(boolean_nodes)

#Multivalued nodes getter (not input)
def getInternalMultivaluedNodes(nodes):
    multivalued_nodes = []
    for node in nodes:
        if node[1] > 1 and not node[2]:
            multivalued_nodes += [(str(node[0]),node[1])]
    return(multivalued_nodes)

#Input Getter
def getInputNodes(nodes):
    input_nodes = []
    for node in nodes:
        if node[2]:
            input_nodes += [(str(node[0]),node[1])]
    return(input_nodes)

def label(nodes):
    labels = []
    for node in nodes:
        print(node[0])
        labels += [node[0]]
    return(labels)

def nodeDictionnary(nodes):
    dico = {}
    #Inputs = Label + Maxi
    dico["Inputs"] = getInputNodes(nodes)

    # Internal boolean = Label only
    dico["Boolean"] = getInternalBooleanNodes(nodes)
    # Internal multi = label only
    dico["Multivalued"] = getInternalMultivaluedNodes(nodes)
    return(dico)

def dicoReader(dico):
    for e in dico:
        if len(dico[e])> 0:
            print(e, str(dico[e]))
            print(dico[e][0])
            print(e, type(dico[e][1]))
        else:
            print(e, "None")

def getOrderedNodes(nodes_list):
    ordered_nodes = []
    for e in nodes_list:
        ordered_nodes += [str(e[0])]
    return(ordered_nodes)

def writefile(filename,label_list):
    f = open(filename, 'w')
    for e in label_list:
        f.write(e + "\n")
    f.close()
    return("Done")


def writeCSV(filename, dict):
    int_to_str = lambda int_value: str(int_value)

    f = open(filename, 'w')
    for key, values in dict.items():
         for tuple_value in values:
             csv_row = [key] + list(map(int_to_str, list(tuple_value)))
             f.write(", ".join(csv_row) + "\n")
    f.close()




####### MAIN ######
print("Let's begin by getting the model!!")
model=g.getModel()
nodes = [(node, node.getMax(), node.isInput()) for node in model.getNodeOrder()]
print("Nodes: (True=Input):"+str(nodes))
#print("Nodes: (True=Input):"+str(getOrderedNodes(nodes)))

# print("\n")
# print("Inputs =>" + str(label(getInputNodes(nodes))))
# #print("Inputs =>" + str(getInputNodes(nodes)))
# print("\n")
# print("Boolean =>" + str(getInternalBooleanNodes(nodes)))
# print("\n")
# #print("Multivalued =>" + str(getInternalMultivaluedNodes(nodes)))

print(dicoReader(nodeDictionnary(nodes)))
writeCSV("Model_nodes.csv",nodeDictionnary(nodes) )
writefile("Model_labels.txt", getOrderedNodes(nodes))


print(dicoReader(nodeDictionnary(nodes)))
writeCSV("Model_nodes.csv",nodeDictionnary(nodes) )
writefile("Model_labels.txt", getOrderedNodes(nodes))
